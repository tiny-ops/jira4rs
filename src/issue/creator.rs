use log::{error, info};
use reqwest::blocking::Client;
use reqwest::StatusCode;
use crate::config::JiraConfig;
use crate::error::OperationError;
use crate::issue::JiraIssue;

use serde::Deserialize;
use crate::issue::request::{CreateIssueRequest, JiraUser, PropertyWithId, RequestFields};

pub struct JiraIssueCreator {
    config: JiraConfig,
    client: Client
}

impl JiraIssueCreator {
    pub fn new(config: &JiraConfig, client: &Client) -> JiraIssueCreator{
        JiraIssueCreator {
            config: config.clone(),
            client: client.clone()
        }
    }

    pub fn create(&self, project_id: &str,
                  jira_issue: &JiraIssue) -> Result<JiraIssue, OperationError> {

        info!("create jira issue, project id '{project_id}'");
        info!("{:?}", jira_issue);

        let request_url = format!("{}/rest/api/2/issue", self.config.base_url);

        let request = CreateIssueRequest {
            fields: RequestFields {
                summary: jira_issue.summary.to_string(),
                description: jira_issue.description.to_string(),
                issue_type: PropertyWithId { id: jira_issue.issue_type_code.to_string() },
                project: PropertyWithId { id: project_id.to_string() },
                assignee: JiraUser {
                    name: jira_issue.assignee.to_string()
                },
                reporter: JiraUser { name: self.config.username.to_string() },
                priority: PropertyWithId { id: jira_issue.priority_id.to_string() },
                labels: vec![]
            }
        };

        match self.client.post(request_url)
            .basic_auth(self.config.username.to_string(), Some(self.config.password.to_string()))
            .json(&request)
            .send() {
            Ok(resp) => {
                let status = resp.status();

                info!("server response code: {}", status);

                match status {
                    StatusCode::CREATED => {

                        match resp.json::<CreateJiraIssueResponse>() {
                            Ok(body) => {
                                info!("jira issue has been created");

                                let mut result = jira_issue.clone();
                                result.id = body.id;
                                result.key = body.key;
                                result.url = body.url;

                                info!("{:?}", result);

                                Ok(result)
                            }
                            Err(e) => {
                                error!("{}", e);
                                Err(OperationError::UnsupportedApiError)
                            }
                        }
                    }
                    StatusCode::BAD_REQUEST => Err(OperationError::BadRequestError),
                    StatusCode::FORBIDDEN => Err(OperationError::ApiPermissionsError),
                    StatusCode::UNAUTHORIZED => Err(OperationError::Authentication),
                    _ => Err(OperationError::UnsupportedApiError)
                }

            }
            Err(e) => {
                error!("{}", e);
                Err(OperationError::ConnectionError(e))
            }
        }
    }
}

#[derive(Deserialize)]
struct CreateJiraIssueResponse {
    pub id: String,
    pub key: String,

    #[serde(rename(deserialize = "self"))]
    pub url: String
}

#[cfg(test)]
mod create_jira_issue_tests {
    use fake::{Fake, Faker};
    use fake::faker::job::en::Title;
    use log::LevelFilter;
    use reqwest::blocking::Client;
    use crate::error::OperationError;
    use crate::issue::builder::JiraIssueBuilder;
    use crate::issue::creator::JiraIssueCreator;
    use crate::issue::JiraIssue;
    use crate::test_helpers::JiraSamples;

    fn init() {
        let _ = env_logger::builder()
            .filter_level(LevelFilter::Debug).is_test(true).try_init();
    }

    #[test]
    fn return_jira_issue() {
        init();

        let jira_config = JiraSamples::get_jira_config();

        let client = Client::new();

        let creator = JiraIssueCreator::new(&jira_config, &client);

        let summary = Title().fake::<String>();

        let issue = get_jira_issue(&summary);

        match creator.create(&JiraSamples::get_project_id(), &issue) {
            Ok(new_issue) => {
                assert!(!new_issue.id.is_empty());
                assert!(!new_issue.key.is_empty());
                assert!(!new_issue.url.is_empty());
            },
            Err(e) => {
                println!("error: {:?}", e);
                panic!("result expected");
            },
        }
    }

    #[test]
    fn return_authentication_error_for_invalid_password() {
        init();

        let password: String = Faker.fake::<String>();

        let mut jira_config = JiraSamples::get_jira_config();
        jira_config.password = password;

        let client = Client::new();

        let creator = JiraIssueCreator::new(&jira_config, &client);

        let summary: String = Faker.fake::<String>();

        let issue = get_jira_issue(&summary);

        match creator.create(&JiraSamples::get_project_id(), &issue) {
            Ok(_) => panic!("error expected"),
            Err(e) => {
                println!("error: {:?}", e);

                match e {
                    OperationError::Authentication => {}
                    _ => panic!("Authentication error expected")
                }
            },
        }
    }

    #[test]
    fn return_authentication_error_for_invalid_username() {
        init();

        let username: String = Faker.fake::<String>();

        let mut jira_config = JiraSamples::get_jira_config();
        jira_config.username = username;

        let client = Client::new();

        let creator = JiraIssueCreator::new(&jira_config, &client);

        let summary: String = Faker.fake::<String>();

        let issue = get_jira_issue(&summary);

        match creator.create(&JiraSamples::get_project_id(), &issue) {
            Ok(_) => panic!("error expected"),
            Err(e) => {
                println!("error: {:?}", e);

                match e {
                    OperationError::Authentication => {}
                    _ => panic!("Authentication expected")
                }
            },
        }
    }

    #[test]
    fn return_connection_error_for_invalid_url() {
        init();

        let base_url: String = Faker.fake::<String>();

        let mut jira_config = JiraSamples::get_jira_config();
        jira_config.base_url = base_url;

        let client = Client::new();

        let creator = JiraIssueCreator::new(&jira_config, &client);

        let summary = Title().fake::<String>();

        let issue = get_jira_issue(&summary);

        match creator.create(&JiraSamples::get_project_id(), &issue) {
            Ok(_) => panic!("error expected"),
            Err(e) => {
                println!("error: {:?}", e);

                match e {
                    OperationError::ConnectionError(_) => {}
                    _ => panic!("connection error expected")
                }
            },
        }
    }

    fn get_jira_issue(summary: &str) -> JiraIssue {
        let description: String = Faker.fake::<String>();

        let assignee: &str = env!["JIRA_ASSIGNEE"];
        let issue_type: &str = env!["JIRA_ISSUE_TYPE"];
        let priority_id: &str = env!["JIRA_PRIORITY_ID"];

        JiraIssueBuilder::new()
            .summary(&summary).description(&description)
            .assignee(&assignee)
            .issue_type(&issue_type)
            .priority_id(&priority_id)
            .build().unwrap()
    }
}
