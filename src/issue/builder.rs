use log::error;
use crate::error::OperationError;
use crate::issue::JiraIssue;

pub struct JiraIssueBuilder {
    summary: String,
    description: String,
    assignee: String,
    issue_type_code: String,
    priority_id: String,
}

impl JiraIssueBuilder {
    pub fn new() -> JiraIssueBuilder {
        JiraIssueBuilder {
            summary: "".to_string(),
            description: "".to_string(),
            assignee: "".to_string(),
            issue_type_code: "".to_string(),
            priority_id: "".to_string()
        }
    }

    pub fn summary(&mut self, summary: &str) -> &mut Self {
        self.summary = summary.to_string();
        self
    }

    pub fn description(&mut self, description: &str) -> &mut Self {
        self.description = description.to_string();
        self
    }

    pub fn assignee(&mut self, assignee: &str) -> &mut Self {
        self.assignee = assignee.to_string();
        self
    }

    pub fn issue_type(&mut self, issue_type_code: &str) -> &mut Self {
        self.issue_type_code = issue_type_code.to_string();
        self
    }

    pub fn priority_id(&mut self, priority_id: &str) -> &mut Self {
        self.priority_id = priority_id.to_string();
        self
    }

    pub fn build(&self) -> Result<JiraIssue, OperationError> {
        let mut valid = true;

        if self.summary.is_empty() {
            error!("summary cannot be blank");
            valid = false;
        }

        if self.issue_type_code.is_empty() {
            error!("type code cannot be blank");
            valid = false;
        }

        if self.assignee.is_empty() {
            error!("assignee cannot be blank");
            valid = false;
        }

        if self.priority_id.is_empty() {
            error!("priority id cannot be blank");
            valid = false;
        }

        if valid {
            Ok(
                JiraIssue {
                    id: "".to_string(),
                    key: "".to_string(),
                    summary: self.summary.to_string(),
                    description: self.description.to_string(),
                    issue_type_code: self.issue_type_code.to_string(),
                    assignee: self.assignee.to_string(),
                    priority_id: self.priority_id.to_string(),
                    url: "".to_string()
                }
            )

        } else { Err(OperationError::BadRequestError) }
    }
}

#[cfg(test)]
mod jira_issue_builder_tests {
    use fake::{Fake, Faker};
    use crate::error::OperationError;
    use crate::issue::builder::JiraIssueBuilder;
    use crate::issue::JiraIssue;

    #[test]
    fn return_jira_issue_struct() {
        let mut builder = JiraIssueBuilder::new();

        let summary: String = Faker.fake::<String>();
        let description: String = Faker.fake::<String>();
        let issue_type_code: String = Faker.fake::<String>();
        let assignee: String = Faker.fake::<String>();
        let priority_id: String = Faker.fake::<String>();

        match builder
            .summary(&summary)
            .description(&description)
            .assignee(&assignee)
            .issue_type(&issue_type_code)
            .priority_id(&priority_id)
            .build() {
            Ok(jira_issue) => {
                let expected_jira_issue = JiraIssue {
                    id: "".to_string(),
                    key: "".to_string(),
                    summary,
                    description,
                    issue_type_code,
                    assignee,
                    priority_id,
                    url: "".to_string()
                };

                assert_eq!(expected_jira_issue, jira_issue);
            },
            Err(_) => panic!("unexpected.error")
        }
    }

    #[test]
    fn return_error_for_blank_assignee_value() {
        let mut builder = JiraIssueBuilder::new();

        let summary: String = Faker.fake::<String>();
        let description: String = Faker.fake::<String>();
        let issue_type_code: String = Faker.fake::<String>();
        let priority_id: String = Faker.fake::<String>();

        match builder
            .summary(&summary)
            .description(&description)
            .assignee("")
            .issue_type(&issue_type_code)
            .priority_id(&priority_id)
            .build() {
            Ok(_) => panic!("error expected"),
            Err(e) => assert_bad_request_error(e)
        }
    }

    #[test]
    fn return_error_for_blank_issue_type_code() {
        let mut builder = JiraIssueBuilder::new();

        let summary: String = Faker.fake::<String>();
        let description: String = Faker.fake::<String>();
        let assignee: String = Faker.fake::<String>();
        let priority_id: String = Faker.fake::<String>();

        match builder
            .summary(&summary)
            .description(&description)
            .assignee(&assignee)
            .issue_type("")
            .priority_id(&priority_id)
            .build() {
            Ok(_) => panic!("error expected"),
            Err(e) => assert_bad_request_error(e)
        }
    }

    #[test]
    fn return_error_for_blank_summary() {
        let mut builder = JiraIssueBuilder::new();

        let description: String = Faker.fake::<String>();
        let assignee: String = Faker.fake::<String>();
        let issue_type: String = Faker.fake::<String>();
        let priority_id: String = Faker.fake::<String>();

        match builder.summary("")
            .description(&description)
            .assignee(&assignee)
            .priority_id(&priority_id)
            .issue_type(&issue_type)
            .build() {
            Ok(_) => panic!("error expected"),
            Err(e) => assert_bad_request_error(e)
        }
    }

    #[test]
    fn return_error_for_blank_priority_id() {
        let mut builder = JiraIssueBuilder::new();

        let summary: String = Faker.fake::<String>();
        let description: String = Faker.fake::<String>();
        let assignee: String = Faker.fake::<String>();
        let issue_type: String = Faker.fake::<String>();

        match builder.summary(&summary)
            .description(&description)
            .assignee(&assignee)
            .issue_type(&issue_type)
            .priority_id("")
            .build() {
            Ok(_) => panic!("error expected"),
            Err(e) => assert_bad_request_error(e)
        }
    }

    fn assert_bad_request_error(e: OperationError) {
        match e {
            OperationError::BadRequestError => assert!(true),
            _ => panic!("unexpected error type")
        }
    }
}
