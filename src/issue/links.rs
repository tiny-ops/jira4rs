use log::{debug, error, info};
use reqwest::blocking::Client;
use reqwest::StatusCode;
use crate::config::JiraConfig;
use crate::error::OperationError;

use serde::Serialize;

pub struct JiraLinker {
    config: JiraConfig,
    client: Client
}

impl JiraLinker {
    pub fn new(config: &JiraConfig, client: &Client) -> JiraLinker {
        JiraLinker {
            config: config.clone(),
            client: client.clone()
        }
    }

    /// Assign url to Jira issue
    ///
    /// - `issue_id` - issue id (integer value, i.e. 49888) or key (`PRJ-123`)
    /// - `title` - title for url
    /// - `url` - url for assign
    ///
    /// API: [Create or update remote issue link](https://docs.atlassian.com/software/jira/docs/api/REST/7.6.1/#api/2/issue-createOrUpdateRemoteIssueLink)
    ///
    pub fn assign_url_to_issue(&self, issue_id: &str, title: &str, url: &str) -> Result<(), OperationError> {
        info!("assign url '{}' to jira issue '{}'", url, issue_id);
        info!("with title '{}'", title);

        let request_url = format!(
            "{}/rest/api/2/issue/{}/remotelink", self.config.base_url, issue_id
        );

        debug!("request url '{}'", request_url);

        let request = AssignUrlRequest {
            object: RemoteLinkObject {
                url: url.to_string(), title: title.to_string()
            }
        };

        match self.client.post(request_url)
            .basic_auth(
                self.config.username.to_string(),
                Some(self.config.password.to_string())
            )
            .json(&request)
            .send() {
            Ok(resp) => {
                let status = resp.status();

                info!("server response code: {}", status);

                match status {
                    StatusCode::CREATED => {
                        info!("url '{}' has been attached to jira issue '{}'", url, issue_id);
                        Ok(())
                    }
                    StatusCode::BAD_REQUEST => Err(OperationError::BadRequestError),
                    StatusCode::FORBIDDEN => Err(OperationError::ApiPermissionsError),
                    StatusCode::UNAUTHORIZED => Err(OperationError::Authentication),
                    _ => Err(OperationError::UnsupportedApiError)
                }

            }
            Err(e) => {
                error!("{}", e);
                Err(OperationError::ConnectionError(e))
            }
        }
    }
}

#[derive(Serialize,Clone,Debug)]
struct AssignUrlRequest {
    object: RemoteLinkObject
}

#[derive(Serialize,Clone,Debug)]
struct RemoteLinkObject {
    title: String,
    url: String,
}

#[cfg(test)]
mod tests {
    use fake::{Fake, Faker};
    use log::LevelFilter;
    use reqwest::blocking::Client;
    use crate::error::OperationError;
    use crate::issue::links::JiraLinker;
    use crate::test_helpers::JiraSamples;

    const SAMPLE_URL: &str = "http://localhost";

    fn init() {
        let _ = env_logger::builder()
            .filter_level(LevelFilter::Debug)
            .is_test(true).try_init();
    }

    #[test]
    fn return_ok_after_success() {
        init();

        let jira_config = JiraSamples::get_jira_config();

        let client = Client::new();

        let linker = JiraLinker::new(&jira_config, &client);

        let issue_id = JiraSamples::get_issue_id();

        let url_title: String = Faker.fake::<String>();

        assert!(linker.assign_url_to_issue(&issue_id, &url_title, SAMPLE_URL).is_ok());
    }

    #[test]
    fn return_connection_error_for_invalid_url() {
        init();

        let mut jira_config = JiraSamples::get_jira_config();
        jira_config.base_url = Faker.fake::<String>();

        let client = Client::new();

        let linker = JiraLinker::new(&jira_config, &client);

        let issue_key = JiraSamples::get_issue_id();

        let url_title: String = Faker.fake::<String>();

        match linker.assign_url_to_issue(
            &issue_key, &url_title, SAMPLE_URL
        ) {
            Ok(_) => panic!("ConnectionError expected"),
            Err(e) => {
                println!("error: {:?}", e);

                match e {
                    OperationError::ConnectionError(_) => {}
                    _ => panic!("Authentication error expected")
                }
            },
        }
    }

    #[test]
    fn return_authentication_error_for_invalid_username() {
        init();

        let mut jira_config = JiraSamples::get_jira_config();
        jira_config.username = Faker.fake::<String>();

        let client = Client::new();

        let linker = JiraLinker::new(&jira_config, &client);

        let issue_key = JiraSamples::get_issue_id();

        let url_title: String = Faker.fake::<String>();

        match linker.assign_url_to_issue(
            &issue_key, &url_title, SAMPLE_URL
        ) {
            Ok(_) => panic!("error expected"),
            Err(e) => {
                println!("error: {:?}", e);

                match e {
                    OperationError::Authentication => {}
                    _ => panic!("Authentication error expected")
                }
            },
        }
    }

    #[test]
    fn return_authentication_error_for_invalid_password() {
        init();

        let client = Client::new();

        let mut jira_config = JiraSamples::get_jira_config();
        jira_config.password = Faker.fake::<String>();

        let linker = JiraLinker::new(&jira_config, &client);

        let issue_key = JiraSamples::get_issue_id();

        let url_title: String = Faker.fake::<String>();

        match linker.assign_url_to_issue(
            &issue_key, &url_title, SAMPLE_URL
        ) {
            Ok(_) => panic!("error expected"),
            Err(e) => {
                println!("error: {:?}", e);

                match e {
                    OperationError::Authentication => {}
                    _ => panic!("Authentication error expected")
                }
            },
        }
    }
}
