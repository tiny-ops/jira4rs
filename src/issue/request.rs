use serde::{Serialize};

#[derive(Serialize,Clone,Debug)]
pub struct CreateIssueRequest {
    pub fields: RequestFields
}

#[derive(Serialize,Clone,Debug)]
pub struct RequestFields {
    pub summary: String,
    pub description: String,

    #[serde(rename(serialize = "issuetype"))]
    pub issue_type: PropertyWithId,

    pub project: PropertyWithId,

    pub assignee: JiraUser,

    pub reporter: JiraUser,

    pub priority: PropertyWithId,

    pub labels: Vec<String>
}

#[derive(Serialize,Clone,Debug)]
pub struct PropertyWithId {
    pub id: String
}

#[derive(Serialize,Clone,Debug)]
pub struct JiraUser {
    pub name: String
}


