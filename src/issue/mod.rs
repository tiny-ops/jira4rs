pub mod builder;

mod request;
pub mod creator;
pub mod links;

use serde::{Serialize, Deserialize};

#[derive(Serialize,Deserialize,Clone,Debug, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct JiraIssue {
    pub id: String,
    pub key: String,
    pub summary: String,
    pub description: String,
    pub issue_type_code: String,
    pub assignee: String,
    pub priority_id: String,
    pub url: String
}

#[derive(Serialize,Deserialize,Clone,Debug)]
#[serde(rename_all = "camelCase")]
pub struct JiraAssignee {
    pub user_name: String
}
