use thiserror::Error;

#[derive(Error, Debug)]
pub enum OperationError {
    #[error("bad request error")]
    BadRequestError,

    #[error("network error")]
    ConnectionError(#[from] reqwest::Error),

    #[error("permissions error")]
    ApiPermissionsError,

    #[error("unsupported api error")]
    UnsupportedApiError,

    #[error("authentication error")]
    Authentication
}
