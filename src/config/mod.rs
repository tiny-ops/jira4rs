#[derive(Clone,Debug)]
pub struct JiraConfig {
    pub base_url: String,
    pub username: String,
    pub password: String
}
