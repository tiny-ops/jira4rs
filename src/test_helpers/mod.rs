use crate::config::JiraConfig;

pub struct JiraSamples;

impl JiraSamples {
    pub fn get_project_id() -> String {
        env!["JIRA_PROJECT_ID"].to_string()
    }

    pub fn get_jira_config() -> JiraConfig {
        let base_url = env!["JIRA_BASE_URL"];
        let username = env!["JIRA_USERNAME"];
        let password = env!["JIRA_PASSWORD"];

        JiraConfig {
            base_url: base_url.to_string(),
            username: username.to_string(),
            password: password.to_string()
        }
    }

    pub fn get_issue_id() -> String {
        env!["JIRA_ISSUE_ID"].to_string()
    }
}
