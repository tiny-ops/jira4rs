# jira4rs

NO WARRANTY, "AS IS" LICENSE :))

## How to use

### 1. Create issue

1. Use `JiraIssueBuilder` to build `JiraIssue` entity.
2. Then use `JiraIssueCreator` to create an issue.

### 2. Assign URL to issue

Use `JiraLinker`.

## Development

Define environment variables for integration tests:

- `JIRA_BASE_URL`
- `JIRA_USERNAME`
- `JIRA_PASSWORD`
- `JIRA_PROJECT_ID`
- `JIRA_ASSIGNEE`
- `JIRA_ISSUE_TYPE`
- `JIRA_PRIORITY_ID`
- `JIRA_ISSUE_ID` - Example value: 49888 or key PRJ-123

## Official Jira References

- API - https://docs.atlassian.com/software/jira/docs/api/REST/7.6.1/

- Example for Jira API - https://developer.atlassian.com/server/jira/platform/jira-rest-api-examples/


## Troubleshooting

### Jira API respond with 403 Forbidden

User has too many failure login attempts. Just reset his attempts counter Admin area -> User settings.
